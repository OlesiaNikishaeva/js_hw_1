// 1
let name;
let admin;
name = "Olesya";
admin = name;
console.log(admin);

// 2
let days = 5;
let seconds = days*24*60*60;
console.log(seconds);
// -or-
let day = 5;
console.log(days*24*60*60);

// 3
let userName = prompt("What is your name?");
console.log(userName);